//Proyecto Bootcamp Practitioner 3ed MX

var express = require('express');
var bcrypt = require('bcrypt');
var requestJson = require('request-json');
var bodyparser = require('body-parser');
var cors = require('cors');
var urlMLabClients = 'https://api.mlab.com/api/1/databases/mgarcia/collections/Clients?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
var urlMLabRaiz = 'https://api.mlab.com/api/1/databases/mgarcia/collections/';
var clientMlabRaiz;
var clientProyect = requestJson.createClient(urlMLabClients);
var apiKey = 'apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt';
 var SALT_ROUNDS = 12;

//Inicializando
app = express(),
port = process.env.PORT || 3000;
app.use(bodyparser.json());
console.log('Services starting');
console.log('Loading Access-Control');
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Header","Origin, X-Requested-With, Content-Type, Accept");
  next();
})
console.log('Define port');
app.listen(port);
console.log('Start Cross Domain validator');
//Permitiendo Cors en localhost
var allowedOrigins = ['http://localhost:8081',
                      'http://localhost:3001',
                      'http://127.0.0.1:8081',
                      'http://127.0.0.1:3001'];
app.use(cors({
  origin: function(origin, callback){
    // allow requests with no origin
    // (like mobile apps or curl requests)
    console.log(origin);
    if(!origin) return callback(null, true);
    if(allowedOrigins.indexOf(origin) === -1){
      console.error(msg);
      var msg = 'The CORS policy for this site does not ' +
                'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  }
}));
console.log('RESTful API server started on: ' + port);
//

/*
API para la consulta del Cliente especificado con validación de Token de seguridad
el cual debe ser enviado en el Header de la solicitud
*/
app.get('/v1/Clients/:idclient', function(req,res) {
    var query = '&q={"idclient":'+req.params.idclient  +'}'
    var filter = '&f={"first_name":1,' +
    '"last_name":1,'+
    '"email":1,' +
    '"user_name":1, ' +
    '"accounts.account":1,' +
    '"accounts.card":1,' +
    '"security.token":1,'+
    '"_id":0}'
    var uri = urlMLabClients + query + filter;
    console.log(uri);
    clientProyect.get(uri,function(err, resM, body) {
      if (err) {
        console.error('Error, petición vacia');
        console.error(body);
        res.status(400).json({"errorMsj":"Error, petición vacia"})
      } else {
        if (body.length >= 1) {
          console.log(JSON.stringify(body));
          console.log(body[0].security.token);
          console.log(req.headers.token);
          if (body[0].security.token == req.headers.token) {
            res.status(200).send(body);
          } else {
            res.status(403).json({"errorMsj":"Credenciales no validas"})
          }

        } else {
          res.status(403).json({"errorMsj":"Error, petición vacia"})
        }

      }

    })
})
/*
API para realizar el login dentro del sistema mediante usuario y contraseña
Se genera un Token cuando las credenciales son correctas y este se almacena en la
Base de Datos Mongo para después servir como validador de seguridad.
*/
app.post('/v3/login',function(req, res) {
  console.log('Init Login');
  var username = req.body.username;
  var pass = req.body.password;
  console.log('Username: ' + username);
  var jsonquery = '{"security.username":"' + username + '"}';
  var jsonResp = '&f={"first_name":1,' +
  '"last_name":1,'+
  '"email":1,' +
  '"security.username":1, ' +
  '"idclient":1, ' +
  '"security.password":1, ' +
  '"_id":1},';
  var query = urlMLabRaiz + "Clients?" + apiKey +'&q=' + jsonquery + jsonResp;
  clientMlabRaiz = requestJson.createClient( query );
  clientMlabRaiz.get('',function(err, resM, body) {
    if (!err && body.length == 1) {//Login OK
      var today = new Date();
      var jsonBody = JSON.stringify(body);
      console.log("Client found: " + jsonBody);
      //Compara password
      var passwordResp = body[0].security.password;
      console.log(passwordResp);
      console.log(pass);
      console.log(bcrypt.compareSync(pass, passwordResp));
      if(bcrypt.compareSync(pass, passwordResp)){
        //Generando Token
        let hash = bcrypt.hashSync(jsonBody + today,SALT_ROUNDS);
        jsonBody = JSON.parse(jsonBody);
        var id = jsonBody[0]._id.$oid;
        var idClient = jsonBody[0].idclient;
        var updateQuery = urlMLabRaiz + "Clients/"+ id + '?' + apiKey ;
        //console.log(updateQuery);
        clientMlabRaiz = requestJson.createClient(updateQuery);
        var data = JSON.stringify( { "$set" : { "security.token" : hash, "security.access_date" : today } } );
        clientMlabRaiz.put('',JSON.parse(data),function(err, resM, body) {
          if (!err) {
            console.log("Token actualizado en BD");
            //console.log(idClient);
            res.status(200).json({
                "token" : hash,
                "idClient" : idClient
              });
          }else{
            console.log(body);
            res.status(500).json({"errorMsj" : "No se puede generar Token"});
          }
        })
      }else{
        console.error('Credenciales invalidas');
        res.status(404).json({"errorMsj" : "Credenciales invalidas"});
      }
    } else {
      console.error('Credenciales invalidas');
      res.status(404).json({"errorMsj" : "Credenciales invalidas"});
    }
  })
})
/*
API para la consulta de los movimientos de una cuenta en específica.
Se valida el token en el header
*/
app.get('/v1/Clients/:idclient/Movments/:account', function(req,res) {
    var query = '&q={"idclient":'+req.params.idclient+
      ',"accounts.account":"' + req.params.account + '"}'
    var filter = '&f={' +
      '"_id":0,' +
      '"security.token":1, ' +
      '"accounts.$":1}'
    var uri = urlMLabClients + query + filter;
    clientProyect.get(uri,function(err, resM, body) {
      if (err) {
        console.error('Error, petición vacia');
        console.error(body);
        res.status(400).json({"errorMsj":"Error, petición vacia"})
      } else {
        if (body.length >= 1) {
          //console.log(JSON.stringify(body));
          //var resp = JSON.parse(body);
          //console.log(body);
          if (body[0].security.token == req.headers.token) {
            res.status(200).send(body);
          } else {
            res.status(403).json({"errorMsj":"Credenciales no validas"})
          }

        } else {
          res.status(403).json({"errorMsj":"Error, petición vacia"})
        }

      }

    })
})
/*
API para el ingreso de nuevos movimientos para una cuenta especificada.
Se valida el Token de seguridad en el Header
*/
app.put('/v1/Clients/:idclient/Movments/:account',function(req, res) {
  var query = '&q={"idclient":'+req.params.idclient+
    ',"accounts.account":"' + req.params.account +
    '", "security.token": "' + req.headers.token +
    '"}'
    var uri = urlMLabClients + query;
    clientMlabRaiz = requestJson.createClient(uri);
    var data = JSON.stringify( { "$push" : {"accounts.$.movments" :req.body} } );
    console.log(data);
    clientMlabRaiz.put('',JSON.parse(data),function(err, resM, body) {
      if (!err) {
        console.log("Movimiento agregado");
        res.status(200).send(body)
      }else{
        console.log(body);
        res.status(403).json({"errorMsj":"Error, actualización no realizada"})
      }
    })

})

/*
API para dar de alta un nuevo cliente
*/
app.post('/v1/Clients', function(req, res) {
  var query = '&q={"idclient":'+req.body.idclient +
              ', "first_name": "' + req.body.firstName +
              '", "last_name": "' + req.body.lastName +
              '", "email": "' + req.body.email +
              '"}';
  var passClear = req.body.password;
  console.log('PassClear: ' + passClear);
  var hashPass = bcrypt.hashSync(passClear,SALT_ROUNDS);
  console.log('Hash: ' + hashPass);
  var addData = JSON.stringify({"$set" : {"security.username":req.body.username,
    "security.password":hashPass,"security.token":"", "security.access_date":""}});
  var uri = urlMLabClients + query ;
  clientMlabRaiz = requestJson.createClient(uri);
  clientMlabRaiz.put('',JSON.parse(addData), function(err, resM, body) {
    if (!err) {
      console.log(body.n);
      var result = body.n;
      if (result > 0) {
        res.status(200).json({"idclient":req.body.idclient, "username" : req.body.username, "msj" : "Cliente Registrado"});
      } else {
        res.status(403).json({"errorMsj":"Error, Cliente no valido"})
      }

    } else {
      console.log(body);
      res.status(403).json({"errorMsj":"Error, Cliente no valido"})
    }
  })
})
